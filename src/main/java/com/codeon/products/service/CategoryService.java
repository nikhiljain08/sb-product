package com.codeon.products.service;

import com.codeon.products.entity.Category;
import com.codeon.products.entity.Product;
import com.codeon.products.repository.CategoryRepository;
import com.codeon.products.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository repository;

    public Category addCategory(Category category) {
        return repository.save(category);
    }

}
