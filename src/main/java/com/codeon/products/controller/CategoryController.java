package com.codeon.products.controller;

import com.codeon.products.entity.Category;
import com.codeon.products.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService service;

    @GetMapping("/")
    public String welcome() {
        return "Category is active!!";
    }

    @PostMapping("/add")
    public Category addCategory(@RequestBody Category category) {
        return service.addCategory(category);
    }
}
