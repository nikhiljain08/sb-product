package com.codeon.products.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "PRODUCT_TB")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Product {

    @Id
    private String _id;
    private String name;
    private double base_price;
    private boolean active;
    private int cur_qty;
    private int base_qty;
    private String qty_type;
    private String description;
    private String img_url;
    private double org_price;
    private String category_id;
}
