FROM openjdk:11
ARG JAR_FILE=build/libs/products-0.0.1.jar
COPY ${JAR_FILE} products.jar
ENTRYPOINT ["java","-jar","/products.jar"]